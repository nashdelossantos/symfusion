<?php

namespace Shann\PageBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use Shann\PageBundle\Entity\Page;
use Shann\PageBundle\Form\PageType;

class PageController extends Controller
{
	/**
	 * Page Index
	 * 
	 * @return [type] [description]
	 */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$pages = $em->getRepository('ShannPageBundle:Page')->findAll();

        return $this->render('ShannPageBundle:Admin\Page:index.html.twig', array(
        	'pages'		=> $pages,
        ));
    }

    /**
     * New Page
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request)
    {
    	$page = new Page();
    	$form = $this->createForm(PageType::class, $page);

    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($page);
    		$em->flush();

    		$this->get('session')->getFlashBag()->add('success', 'Page successfully saved!');

    		return $this->redirectToRoute('admin_page_index');
    	}

    	return $this->render('ShannPageBundle:Admin\Page:form.html.twig', array(
    		'form'	=> $form->createView()
    	));
    }

    /**
     * Edit page
     * 
     * @param  Request $request [description]
     * @param  integer $id      [description]
     * @return [type]           [description]
     */
    public function editAction(Request $request, $id = 0)
    {
        if (!$id) {
            throw new NotFoundHttpException('Page ID Not Valid!');
        }

        $em = $this->getDoctrine()->getManager();

        if (!$page = $em->getRepository('ShannPageBundle:Page')->findOneById($id)) {
            throw new NotFoundHttpException('Page Not Found!');
        }

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($page);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Page successfully modififed!');

            return $this->redirectToRoute('admin_page_index');
        }

        return $this->render('ShannPageBundle:Admin\Page:form.html.twig', array(
            'form'  => $form->createView(),
            'page'  => $page,
        ));
    }

    /**
     * Delete a page
     * 
     * @param  Request $request [description]
     * @param  integer $id      [description]
     * @return [type]           [description]
     */
    public function deleteAction(Request $request, $id = 0)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Method Not Allowed!',
                'text'          => 'Please contact the admin!',
            ));
        }
        if (!$id) {
           return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Page ID Not Valid!',
                'text'          => 'Please contact the admin!',
            ));
        }

        $em = $this->getDoctrine()->getManager();
        if (!$page = $em->getRepository('ShannPageBundle:Page')->findOneById($id)) {
            return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Page Not Found!',
                'text'          => 'Please contact the admin!',
            ));
        }

        $em->remove($page);
        $em->flush();

        return new JsonResponse(array(
            'success'       => true,
            'title'         => 'Page Deleted!',
            'text'          => 'Page successfully deleted!',
        ));

    }
}
