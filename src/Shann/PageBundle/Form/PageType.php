<?php

namespace Shann\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'attr'                          => array(
                    'class' => 'form-control'
                ),
                'label'     => false
            ))
            ->add('content', CkeditorType::class, array(
                'transformers'                  => array('html_purifier'),
                'toolbar'                       => array('document','basicstyles'),
                'toolbar_groups'                => array(
                    'document' => array('Source')
                ),
                'startup_outline_blocks'        => false,
                'width'                         => '100%',
                'height'                        => '320',
                'label'                         => false,
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shann\PageBundle\Entity\Page'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shann_pagebundle_page';
    }


}
