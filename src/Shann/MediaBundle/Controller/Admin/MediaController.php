<?php

namespace Shann\MediaBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

use Application\Sonata\MediaBundle\Entity\Media;

class MediaController extends Controller
{
    public function indexAction()
    {
    	$mediaManager = $this->container->get('sonata.media.manager.media');

    	$media = $mediaManager->findAll();

    	return $this->render('ShannMediaBundle:Admin\Media:index.html.twig', array(
    		'allMedia'	=> $media,
    	));
    }

    public function uploadImagesAction(Request $request)
    {

    	$mediaManager = $this->container->get('sonata.media.manager.media');

        // Getting sonata media object and saving media
        $media = new Media;
        $media->setBinaryContent($request->files->get('file'));
        $media->setContext('default');
        $media->setProviderName('sonata.media.provider.image');
        $mediaManager->save($media);

        $provider = $this->container->get($media->getProviderName());

        return new JsonResponse(array(
        	'success' 		=> true,
        	'media'			=> $provider->generatePublicUrl($media, 'admin'),
        	'id'			=> $media->getId(),
        ));
    }
}
