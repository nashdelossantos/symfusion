<?php

namespace Shann\PostBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use Cocur\Slugify\Slugify;

use Shann\PostBundle\Form\PostType;
use Shann\PostBundle\Form\CategoryType;
use Shann\PostBundle\Entity\Post;
use Shann\TaxonomyBundle\Entity\Taxonomy;
use Shann\TaxonomyBundle\Form\TaxonomyType;

class PostController extends Controller
{
	/**
	 * Displays all posts
	 *
	 * @return [type] [description]
	 */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('ShannPostBundle:Post')->findBy([],[]);

        return $this->render('ShannPostBundle:Admin\Post:index.html.twig', array(
            'posts' => $posts,
        ));
    }

    /**
     * New Post
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {

            if ($categories = $request->request->get('categories')) {
                // clear previous categories and add new
                $post->getCategories()->clear();
                foreach ($categories as $category) {
                    $newCategory = $em->getRepository('ShannTaxonomyBundle:Taxonomy')->findOneById($category);
                    $post->addCategory($newCategory);
                }
            }

            $em->persist($post);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Post successfully saved!');

            return $this->redirectToRoute('post_index');
        }


        $categories = $em->getRepository('ShannTaxonomyBundle:Taxonomy')->findBy(['type' => 'post'], ['name' => 'ASC']);

    	return $this->render('ShannPostBundle:Admin\Post:form.html.twig', array(
            'form'          => $form->createView(),
            'categories'    => $categories,
        ));
    }

    /**
     * Edit a post
     *
     * @param  Request $request [description]
     * @param  integer $id      [description]
     * @return [type]           [description]
     */
    public function editAction(Request $request, $id = 0)
    {
        if (!$id) {
            throw new NotFoundHttpException('Post ID Not Found!');
        }

        $em = $this->getDoctrine()->getManager();
        if (!$post = $em->getRepository('ShannPostBundle:Post')->findOneById($id)) {
            throw new NotFoundHttpException('Post Not Found!');
        }

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($categories = $request->request->get('categories')) {
                // clear previous categories and add new
                $post->getCategories()->clear();
                foreach ($categories as $category) {
                    $newCategory = $em->getRepository('ShannTaxonomyBundle:Taxonomy')->findOneById($category);
                    $post->addCategory($newCategory);
                }
            }
            $em->persist($post);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Post successfully updated!');

            return $this->redirectToRoute('post_index');
        }

        $categories = $em->getRepository('ShannTaxonomyBundle:Taxonomy')->findBy(['type' => 'post'], ['name' => 'ASC']);

        return $this->render('ShannPostBundle:Admin\Post:form.html.twig', array(
            'form'          => $form->createView(),
            'post'          => $post,
            'categories'    => $categories,
            'post'          => $post,
        ));
    }

    /**
     * Delete  a post
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Method Not Allowed!',
                'text'          => 'Please contact the admin!',
            ));
        }

        if (!$id = $request->request->get('id')) {
           return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Post ID Not Found!',
                'text'          => 'Please contact the admin!',
            ));
        }

        $em = $this->getDoctrine()->getManager();
        if (!$post = $em->getRepository('ShannPostBundle:Post')->findOneById($id)) {
            return new JsonResponse(array(
                'error'       => true,
                'title'         => 'Post Not Found!',
                'text'          => 'Please contact the admin!',
            ));
        }

        $em->remove($post);
        $em->flush();

        return new JsonResponse(array(
            'success'       => true,
            'title'         => 'Deleted!',
            'text'          => 'Post successfully deleted!',
        ));
    }

    /**
     * Post Category
     *
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function categoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ShannTaxonomyBundle:Taxonomy')->findBy(['type' => 'post'], ['name' => 'ASC']);

        $category = new Taxonomy();

        $form = $this->createForm(TaxonomyType::class, $category);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $categoryRequest = $request->request->get('shann_taxonomybundle_taxonomy');

            if (!$categoryRequest['slug']) {
                $slug = new Slugify();
                $category->setSlug($slug->slugify($categoryRequest['name']));
            }

            $category->setType('post');

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('post_categories');
        }

        return $this->render('ShannPostBundle:Admin\Post:category.html.twig', array(
            'categories'    => $categories,
            'form'          => $form->createView(),
        ));
    }
}
