<?php

namespace Shann\TaxonomyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class TaxonomyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', HiddenType::class, array())
            ->add('name', TextType::class, array(
                'attr'                          => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('slug', TextType::class, array(
                'required'  => false,
                'label'     => 'Slug (optional)',
                'attr'                          => array(
                    'class' => 'form-control'
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shann\TaxonomyBundle\Entity\Taxonomy'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shann_taxonomybundle_taxonomy';
    }


}
