<?php

namespace Shann\TaxonomyBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TaxonomyController extends Controller
{
    public function indexAction()
    {
        return $this->render('ShannTaxonomyBundle:Default:index.html.twig');
    }
}
