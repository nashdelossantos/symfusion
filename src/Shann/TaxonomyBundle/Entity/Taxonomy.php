<?php

namespace Shann\TaxonomyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taxonomy
 *
 * @ORM\Table(name="taxonomy")
 * @ORM\Entity(repositoryClass="Shann\TaxonomyBundle\Repository\TaxonomyRepository")
 */
class Taxonomy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="string", length=90)
     */
    private $type;

    /**
     * @ORM\Column(name="name", type="string", length=90)
     */
    private $name;

    /**
     * @ORM\Column(name="slug", type="string", length=90)
     */
    private $slug;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Get type
    *
    * @return
    */
    public function getType()
    {
        return $this->type;
    }

    /**
    * Set type
    *
    * @return $this
    */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Taxonomy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Taxonomy
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
