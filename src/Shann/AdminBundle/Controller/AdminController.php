<?php

namespace Shann\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('ShannAdminBundle:Admin:index.html.twig');
    }
}
