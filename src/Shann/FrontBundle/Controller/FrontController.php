<?php

namespace Shann\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller
{
	/**
	 * This is the homepage
	 * @return [type] [description]
	 */
    public function indexAction()
    {
        return $this->render('ShannFrontBundle:Front:index.html.twig');
    }
}
